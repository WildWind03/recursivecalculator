package ru.nsu.chirikhin.calculator;

import org.testng.Assert;
import org.testng.annotations.Test;


public class TokenTest {
    @Test(expectedExceptions = IncompatibleTokenException.class)
    void testInvalidItemException() throws IncompatibleTokenException {
        Token token = new Token(TokenType.LEFT_PARENTHESIS, ")");
    }

    @Test
    void testDataKeeping() throws IncompatibleTokenException {
        Token token = new Token(TokenType.DIVIDE, "/");
        Assert.assertTrue(TokenType.DIVIDE == token.getTokenType());
        Assert.assertEquals("/", token.getTokenRawValue());
    }
}