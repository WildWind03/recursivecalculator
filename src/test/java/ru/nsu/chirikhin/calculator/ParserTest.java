package ru.nsu.chirikhin.calculator;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.StringReader;

public class ParserTest {
    @Test
    public void testParser() {
        try {
            TokenProvider tokenProvider = new TokenProvider(new StringReader("25 + 25@"));
            Parser parser = new Parser(tokenProvider);
            Assert.assertEquals(parser.parseExpression(), 50);

            TokenProvider tokenProvider1 = new TokenProvider(new StringReader("(30 + 25) + 25@"));
            Parser parser1 = new Parser(tokenProvider1);
            Assert.assertEquals(parser1.parseExpression(), 80);
        } catch (TokenGettingException e) {
            e.printStackTrace();
        } catch (IncompatibleTokenException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParserDifficultExpressions() throws TokenGettingException, IncompatibleTokenException {
        TokenProvider tokenProvider = new TokenProvider(new StringReader("((300 - 150) / 10) + 15 - 10^2 + (35 + 35)@"));
        Parser parser = new Parser(tokenProvider);
        Assert.assertEquals(parser.parseExpression(), 0);

        TokenProvider tokenProvider1 = new TokenProvider(new StringReader("10^2^2 + 10^(5 - 5)@"));
        Parser parser1 = new Parser(tokenProvider1);
        Assert.assertEquals(parser1.parseExpression(), 10001);
    }
}