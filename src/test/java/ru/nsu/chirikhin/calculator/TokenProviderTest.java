package ru.nsu.chirikhin.calculator;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.StringReader;

public class TokenProviderTest {
    @Test
    public void testGetLexem() throws Exception {
        TokenProvider tokenProvider = new TokenProvider(new StringReader("5+6@"));
        Assert.assertEquals(tokenProvider.getToken(), new Token(TokenType.NUMBER, "5"));
        Assert.assertEquals(tokenProvider.getToken(), new Token(TokenType.PLUS, "+"));
        Assert.assertEquals(tokenProvider.getToken(), new Token(TokenType.NUMBER, "6"));

        TokenProvider tokenProvider1 = new TokenProvider(new StringReader(("(235 + 27*(238-8888)) / 37@")));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.LEFT_PARENTHESIS, "("));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.NUMBER, "235"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.PLUS, "+"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.NUMBER, "27"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.MULTIPLY, "*"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.LEFT_PARENTHESIS, "("));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.NUMBER, "238"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.MINUS, "-"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.NUMBER, "8888"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.RIGHT_PARENTHESIS, ")"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.RIGHT_PARENTHESIS, ")"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.DIVIDE, "/"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.NUMBER, "37"));
        Assert.assertEquals(tokenProvider1.getToken(), new Token(TokenType.EOF, "@"));

        TokenProvider tokenProvider2 = new TokenProvider(new StringReader("2356              - 16^7           @"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.NUMBER, "2356"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.MINUS, "-"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.NUMBER, "16"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.POWER, "^"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.NUMBER, "7"));
        Assert.assertEquals(tokenProvider2.getToken(), new Token(TokenType.EOF, "@"));
    }

}