package ru.nsu.chirikhin.calculator;

class TokenGettingException extends Exception {

    TokenGettingException(String message) {
        super(message);
    }

    TokenGettingException(Throwable cause) {
        super(cause);
    }
}
