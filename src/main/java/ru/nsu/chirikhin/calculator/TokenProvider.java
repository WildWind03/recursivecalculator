package ru.nsu.chirikhin.calculator;

import java.io.IOException;
import java.io.Reader;

class TokenProvider {

    private final Reader reader;
    private char nextSymbol;

    TokenProvider(Reader reader) throws TokenGettingException {
        this.reader = reader;
        try {
            nextSymbol = (char) reader.read();
        } catch (IOException e) {
           throw new TokenGettingException(e);
        }
    }

    Token getToken() throws TokenGettingException {
        Token currentToken;

        try {

            switch (nextSymbol) {
                case '*':
                    currentToken = new Token(TokenType.MULTIPLY, "*");
                    break;
                case '-':
                    currentToken = new Token(TokenType.MINUS, "-");
                    break;
                case '+':
                    currentToken = new Token(TokenType.PLUS, "+");
                    break;
                case '/':
                    currentToken = new Token(TokenType.DIVIDE, "/");
                    break;
                case '^':
                    currentToken = new Token(TokenType.POWER, "^");
                    break;
                case '(':
                    currentToken = new Token(TokenType.LEFT_PARENTHESIS, "(");
                    break;
                case ')':
                    currentToken = new Token(TokenType.RIGHT_PARENTHESIS, ")");
                    break;
                case '@':
                    currentToken = new Token(TokenType.EOF, "@");
                    break;
                case ' ':
                    try {

                        do {
                            nextSymbol = (char) reader.read();
                        } while (' ' == nextSymbol);

                    } catch (IOException e) {
                        throw new TokenGettingException(e);
                    }

                    return getToken();
                default:
                    if (nextSymbol >= '0' && nextSymbol <= '9') {
                        StringBuilder stringBuilder = new StringBuilder();

                        stringBuilder.append(String.valueOf(nextSymbol));

                        try {
                            while (((nextSymbol = (char) reader.read()) >= '0') && nextSymbol <= '9') {
                                stringBuilder.append(String.valueOf(nextSymbol));
                            }
                        } catch (IOException e) {
                            throw new TokenGettingException(e);
                        }

                        currentToken = new Token(TokenType.NUMBER, stringBuilder.toString());

                        return currentToken;
                    } else {
                        throw new TokenGettingException("Undefined lexem");
                    }
            }
        } catch (IncompatibleTokenException e) {
            throw new TokenGettingException("Trying to create invalid lexem");
        }

        try {
            nextSymbol = (char) reader.read();
        } catch (IOException e) {
            throw new TokenGettingException(e);
        }

        return currentToken;
    }
}
