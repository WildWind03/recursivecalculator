package ru.nsu.chirikhin.calculator;

public class Parser {
    private final TokenProvider tokenProvider;
    private Token nextToken;

    public Parser(TokenProvider tokenProvider) throws IncompatibleTokenException, TokenGettingException {
        this.tokenProvider = tokenProvider;
        nextToken = tokenProvider.getToken();
    }

    public int parseExpression() throws TokenGettingException {
        int totalResult = parseTerm();

        while(TokenType.MINUS == nextToken.getTokenType() || TokenType.PLUS == nextToken.getTokenType()) {

            int sign = TokenType.PLUS == nextToken.getTokenType() ? 1 : -1;

            nextToken = tokenProvider.getToken();
            totalResult += sign * parseTerm();
        }

        return totalResult;
    }

    private int parseTerm() throws TokenGettingException {
        int termValue = parseFactor();

        while (TokenType.DIVIDE == nextToken.getTokenType() || TokenType.MULTIPLY == nextToken.getTokenType()) {
            switch(nextToken.getTokenType()) {
                case MULTIPLY:
                    nextToken = tokenProvider.getToken();

                    termValue*=parseFactor();
                    break;
                case DIVIDE:
                    nextToken = tokenProvider.getToken();

                    termValue/=parseFactor();
                    break;
            }
        }

        return termValue;
    }

    private int parseFactor() throws TokenGettingException {
        int factorValue = parsePower();

        if (TokenType.POWER == nextToken.getTokenType()) {
            nextToken = tokenProvider.getToken();
            return (int) Math.pow(factorValue, parseFactor());
        } else {
            return factorValue;
        }
    }

    private int parsePower() throws TokenGettingException {
        if (TokenType.MINUS == nextToken.getTokenType()) {
            nextToken = tokenProvider.getToken();
            return -1 * parseAtom();
        } else {
            return parseAtom();
        }
    }

    private int parseAtom() throws TokenGettingException {
        if (TokenType.LEFT_PARENTHESIS == nextToken.getTokenType()) {
            nextToken = tokenProvider.getToken();
            int result = parseExpression();
            nextToken = tokenProvider.getToken();
            return result;
        } else {
            int result = Integer.parseInt(nextToken.getTokenRawValue());
            nextToken = tokenProvider.getToken();
            return result;
        }
    }
}
