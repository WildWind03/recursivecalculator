package ru.nsu.chirikhin.calculator;

enum TokenType {
    PLUS, MINUS, MULTIPLY, DIVIDE, POWER, NUMBER, EOF, LEFT_PARENTHESIS, RIGHT_PARENTHESIS
}
