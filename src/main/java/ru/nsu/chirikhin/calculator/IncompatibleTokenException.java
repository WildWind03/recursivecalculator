package ru.nsu.chirikhin.calculator;

class IncompatibleTokenException extends Exception {
   IncompatibleTokenException(String s) {
        super(s);
    }
}
