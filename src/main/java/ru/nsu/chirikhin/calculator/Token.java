package ru.nsu.chirikhin.calculator;

class Token {
    private final TokenType tokenType;
    private final String tokenRawValue;

    Token(TokenType tokenType, String tokenRawValue) throws IncompatibleTokenException {
        switch (tokenType) {
            case LEFT_PARENTHESIS:
                if (!tokenRawValue.equals("(")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case RIGHT_PARENTHESIS:
                if (!tokenRawValue.equals(")")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case PLUS:
                if (!tokenRawValue.equals("+")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case MINUS:
                if (!tokenRawValue.equals("-")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case MULTIPLY:
                if (!tokenRawValue.equals("*")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case DIVIDE:
                if (!tokenRawValue.equals("/")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case POWER:
                if (!tokenRawValue.equals("^")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
            case EOF:
                if (!tokenRawValue.equals("@")) {
                    throw new IncompatibleTokenException(String.format("Type of the tokenRawValue doesn't match to text of the tokenRawValue \n Token Type: %s \n" +
                            " Token: %s \n", tokenType, tokenRawValue));
                }
                break;
        }

        this.tokenType = tokenType;
        this.tokenRawValue = tokenRawValue;
    }

    TokenType getTokenType() {
        return tokenType;
    }

    String getTokenRawValue() {
        return tokenRawValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token1 = (Token) o;

        if (tokenType != token1.tokenType) return false;
        return tokenRawValue != null ? tokenRawValue.equals(token1.tokenRawValue) : token1.tokenRawValue == null;

    }

    @Override
    public int hashCode() {
        int result = tokenType != null ? tokenType.hashCode() : 0;
        result = 31 * result + (tokenRawValue != null ? tokenRawValue.hashCode() : 0);
        return result;
    }
}
